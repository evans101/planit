﻿using System;
using System.Collections.Generic;

using System.Collections.ObjectModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PlanIt
{
    public partial class App : Application
    {
        public static List<UserAccount> user_accounts;
        public static ObservableCollection<Community> communities = new ObservableCollection<Community>();
        public static ObservableCollection<Community> communitiesDisplay { get { return communities; } }
        public static ObservableCollection<Event> events = new ObservableCollection<Event>();
        public static ObservableCollection<Event> eventsDisplay { get { return events; } }

        public static UserAccount accountLoggedIn;
        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new LoginPage());
            user_accounts = new List<UserAccount>();
            createDummyAccountsCommunitiesAndEvents();
            accountLoggedIn = null;
        }

        public static void AddAccount(UserAccount newAccount)
        {
            user_accounts.Add(newAccount);
        }

        public static void AddCommunity(Community newCommunity)
        {
            communities.Add(newCommunity);
        }

        public static void AddEvent(Event newEvent)
        {
            events.Add(newEvent);
        }

        private void createDummyAccountsCommunitiesAndEvents()
        {
            AddAccount(new UserAccount { username = "admin1", password = "123", firstName="Bob", lastName="Noodle" });
            AddAccount(new UserAccount { username = "admin2", password = "123", firstName = "Anne", lastName = "Brown" });
            AddAccount(new UserAccount { username = "admin3", password = "123", firstName = "Noah", lastName = "Peterson" });
            AddAccount(new UserAccount { username="admin4", password="123", firstName="Riley", lastName="Apple" });
            AddCommunity(new Community { name = "Tech Club", ownerName=user_accounts[0].firstName +" " + user_accounts[0].lastName });
            AddCommunity(new Community { name="Basketball Team", ownerName = user_accounts[1].firstName + " " + user_accounts[1].lastName });
            AddCommunity(new Community { name="University Student Union", ownerName = user_accounts[2].firstName + " " + user_accounts[2].lastName });
            AddCommunity(new Community { name="Mobile Programming Class", ownerName = user_accounts[3].firstName + " " + user_accounts[3].lastName });
            AddEvent(new Event { name = "Soccer Game", ownerName = user_accounts[0].firstName + " " + user_accounts[0].lastName , time = "April 23, 6 PM"});
            AddEvent(new Event { name = "Graduation 2020", ownerName = user_accounts[1].firstName + " " + user_accounts[1].lastName, time = "May 17, 10 AM" });
            AddEvent(new Event { name = "Physics Presentation", ownerName = user_accounts[2].firstName + " " + user_accounts[2].lastName, time = "April 28, 1 PM" });
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
