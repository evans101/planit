﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PlanIt
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedMainPage : TabbedPage
    {
        string myUsername, myFirstName, myLastName;
        public static ObservableCollection<Community> communities = App.communities;
        public ObservableCollection<Community> communitiesDisplay = App.communitiesDisplay;
        public static ObservableCollection<Event> events = App.events;
        public ObservableCollection<Event> eventsDisplay = App.eventsDisplay;
        public TabbedMainPage()
        {
            InitializeComponent();
            myUsername = App.accountLoggedIn.username;
            myFirstName = App.accountLoggedIn.firstName;
            myLastName = App.accountLoggedIn.lastName;
        }

        public async void Logout_Clicked(object sender, EventArgs e)
        {
            App.accountLoggedIn = null;
            await Navigation.PopToRootAsync();
        }

        private void OnAppear(object sender, EventArgs e)
        {
            communityList.ItemsSource = App.communities;
            eventList.ItemsSource = App.events;
            myUsername = App.accountLoggedIn.username;
            myFirstName = App.accountLoggedIn.firstName;
            myLastName = App.accountLoggedIn.lastName;
        }
        private void OnDisappear(object sender, EventArgs e)
        {

        }
    }
}