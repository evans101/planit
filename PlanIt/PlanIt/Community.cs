﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlanIt
{
    public class Community
    {
        public string name { get; set; }
        public UserAccount owner { get; set; }
        public string ownerName { get; set; }
        public static List<UserAccount> community_members;
        /*
        public Community(string communityName, UserAccount communityOwner)
        {
            name = communityName;
            owner = communityOwner;
            ownerName = communityOwner.firstName + " " + communityOwner.lastName;
            community_members = new List<UserAccount>();
        }*/

        public static void AddMember(UserAccount member)
        {
            community_members.Add(member);
        }
    }
}
