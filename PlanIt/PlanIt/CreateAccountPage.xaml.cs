﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PlanIt
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateAccountPage : ContentPage
    {
        public CreateAccountPage()
        {
            InitializeComponent();
        }


        async void CreateAccount_Clicked(object sender, EventArgs e)
        {
            string username = ((Entry)entryUsername).Text, 
                password = ((Entry)entryPassword).Text, 
                confirmPassword = ((Entry)entryConfirmPassword).Text,
                firstName = ((Entry)entryFirstName).Text,
                lastName = ((Entry)entryLastName).Text;

            //checks every entry field has content
            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password)
                && !String.IsNullOrEmpty(confirmPassword) && !String.IsNullOrEmpty(firstName)
                && !String.IsNullOrEmpty(lastName))
            {
                bool usernameTaken = false;
                foreach(UserAccount user in App.user_accounts)
                {
                    if(username == user.username)
                    {
                        await DisplayAlert("Error", "That username is taken", "OK");
                        usernameTaken = true;
                        break;
                    }
                }
                if(!usernameTaken)
                {
                    if (password == confirmPassword)
                    {
                        UserAccount createdAccount = new UserAccount { username=username, password=password, firstName=firstName, lastName=lastName };
                        App.AddAccount(createdAccount);
                        App.accountLoggedIn = createdAccount;
                        await Navigation.PushAsync(new TabbedMainPage());
                    }
                    else
                    {
                        await DisplayAlert("Error", "Password and Confirm Password do not match", "OK");
                    }
                }
            }
            else
            {
                await DisplayAlert("Error", "None of the fields can be blank", "OK");
            }
        }
    }
}