﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PlanIt
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        async private void CreateAccount_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CreateAccountPage());
        }

        async private void Login_Clicked(object sender, EventArgs e)
        {
            string login = ((Entry)userLogin).Text;
            string password = ((Entry)userPassword).Text;

            if (!String.IsNullOrEmpty(login) && !String.IsNullOrEmpty(password))
            {
                foreach (UserAccount user in App.user_accounts)
                {
                    if(user.username == login && user.password == password)
                    {
                        App.accountLoggedIn = user;
                        break;
                    }
                }
                
                if(App.accountLoggedIn == null)
                {
                    //invalid
                    await DisplayAlert("Error", "Username or password is incorrect", "OK");
                }
                else
                {
                    await Navigation.PushAsync(new TabbedMainPage());
                }
            }
            else
            {
                await DisplayAlert("Error", "Username or password is blank", "OK");
            }
        }
    }
}