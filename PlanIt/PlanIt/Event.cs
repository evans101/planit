﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlanIt
{
    public class Event
    {
        public string name { get; set; }
        public string ownerName { get; set; }
        public string time { get; set; }
    }
}
