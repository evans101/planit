﻿namespace PlanIt
{ 
    public class Account
    {
        [PrimaryKey, AutoIncrement]
        public string username { get; set; }
        public string password { get; set; }
    }
}
